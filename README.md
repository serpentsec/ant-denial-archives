# Ant-Denial Archives

An archive of evidence maintained to prevent people from denying their own actions and claims.
Know that this project is on GitLab in order to prevent tampering - GitLab keeps commit history forever, even for deleted or modified files.